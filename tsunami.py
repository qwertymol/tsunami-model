import math
import matplotlib.pyplot as plt

lx = 100
dx = 1
X = int(lx / dx)

k = 0.5
n0 = 0.2
b = 0.1
g = 9.8
H = [1 - i / X for i in range(X)]


def model(k, lt, dt):
	ng0 = [0 for i in range(X)]
	u0 = [0 for i in range(X)]
	ng = [0 for i in range(X)]
	u = [0 for i in range(X)]

	T = int(lt / dt)

	for t in range(T):
		# левые граничные условия
		if t <= math.pi * 2 / b:
			ng[0] = 0.5 * n0 * (1 + math.sin(b * t - math.pi / 2))
			u[0] = ng[0] * math.sqrt(g / H[0])
		else:
			ng[0] = ng0[0]
			u[0] = u0[0]

		# правые граничные условия
		u[X - 1] = u0[X - 1] - dt * (u0[X - 1] * (u0[X - 1] - u0[X - 2]) / dx + g * (ng0[X - 1] - ng0[X - 2]) / dx)
		ng[X - 1] = ng0[X - 1] - dt * ((H[X - 1] + ng0[X - 1]) * (u[X - 1] - u[X - 2]) / dx + 
			u[X - 1] * (H[X - 1] + ng0[X - 1] - H[X - 2] - ng0[X - 2]) / dx)

		for x in range(1, X - 1):
			u[x] = u0[x] - dt * (u0[x] * (u0[x + 1] - u0[x - 1]) / (2 * dx) + g * (ng0[x + 1] - ng0[x - 1]) / (2 * dx))

		for x in range(1, X - 1):
			ng[x] = ng0[x] - dt * ((H[x] + ng0[x]) * (u[x + 1] - u[x - 1]) / (2 * dx) + 
				u[x] * (H[x + 1] + ng0[x + 1] - H[x - 1] - ng[x - 1]) / (2 * dx) - 
				k * (ng[x + 1] - 2 * ng[x] + ng[x - 1]) / dx ** 2)

		for x in range(X):
			u0[x] = u[x]
			ng0[x] = ng[x]

	return u

ng = model(0.5, 1, 0.1)
ng1 = model(0.5, 10, 0.1)
ng2 = model(0.5, 40, 0.1)
plt.plot([-h for h in H], label="Дно")
plt.plot(ng, label="T = 1")
plt.plot(ng1, label="T = 10")
plt.plot(ng2, label="T = 40")
plt.legend()
plt.show()